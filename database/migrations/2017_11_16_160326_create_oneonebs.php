<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOneonebs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('Oneonebs');
        Schema::create('Oneonebs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sub_title');
            $table->string('teacher_name');
            $table->string('day');
            $table->string('time');
            $table->string('is_trashed')->default('No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Oneones');
    }
}
