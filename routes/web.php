<?php


/*level one term one section A*/

Route::get('/Oneone/create',function (){

    return view('Oneone/create');
});
Route::post('/Oneone/store', ['uses'=>'OneoneController@store']);
Route::get('/Oneone/index', 'OneoneController@index')->name('Oneoneroutine');
Route::get('/Oneone/index1', 'OneoneController@index1');
Route::get('/Oneone/edit/{id}', ['uses'=>'Oneonecontroller@view4Edit']);
Route::post('/Oneone/update', ['uses'=>'Oneonecontroller@update']);
Route::get('/Oneone/delete/{id}', ['uses'=>'Oneonecontroller@delete']);

/*level one term one section B*/

Route::get('/Oneoneb/create',function (){

    return view('Oneoneb/create');
});
Route::post('/Oneoneb/store', ['uses'=>'OneonebController@store']);
Route::get('/Oneoneb/index', 'OneonebController@index')->name('Oneonebroutine');
Route::get('/Oneoneb/index1', 'OneonebController@index1');
Route::get('/Oneoneb/edit/{id}', ['uses'=>'Oneonebcontroller@view4Edit']);
Route::post('/Oneoneb/update', ['uses'=>'Oneonebcontroller@update']);
Route::get('/Oneoneb/delete/{id}', ['uses'=>'Oneonebcontroller@delete']);

/*level one term two section A*/

Route::get('/Onetwoa/create',function (){

    return view('Onetwoa/create');
});
Route::post('/Onetwoa/store', ['uses'=>'OnetwoaController@store']);
Route::get('/Onetwoa/index', 'OnetwoaController@index')->name('Onetwoaroutine');
Route::get('/Onetwoa/index1', 'OnetwoaController@index1');
Route::get('/Onetwoa/edit/{id}', ['uses'=>'Onetwoacontroller@view4Edit']);
Route::post('/Onetwoa/update', ['uses'=>'Onetwoacontroller@update']);
Route::get('/Onetwoa/delete/{id}', ['uses'=>'Onetwoacontroller@delete']);


/*level one term two section B*/

Route::get('/Onetwob/create',function (){

    return view('Onetwob/create');
});
Route::post('/Onetwob/store', ['uses'=>'OnetwobController@store']);
Route::get('/Onetwob/index', 'OnetwobController@index')->name('Onetwobroutine');
Route::get('/Onetwob/index1', 'OnetwobController@index1');
Route::get('/Onetwob/edit/{id}', ['uses'=>'Onetwobcontroller@view4Edit']);
Route::post('/Onetwob/update', ['uses'=>'Onetwobcontroller@update']);
Route::get('/Onetwob/delete/{id}', ['uses'=>'Onetwobcontroller@delete']);

/*level Two term one section A*/

Route::get('/Twoonea/create',function (){

    return view('Twoonea/create');
});
Route::post('/Twoonea/store', ['uses'=>'TwooneaController@store']);
Route::get('/Twoonea/index', 'TwooneaController@index')->name('Twoonearoutine');
Route::get('/Twoonea/index1', 'TwooneaController@index1');
Route::get('/Twoonea/edit/{id}', ['uses'=>'Twooneacontroller@view4Edit']);
Route::post('/Twoonea/update', ['uses'=>'Twooneacontroller@update']);
Route::get('/Twoonea/delete/{id}', ['uses'=>'Twooneacontroller@delete']);

/*level Two term one section A*/

Route::get('/Twooneb/create',function (){

    return view('Twooneb/create');
});
Route::post('/Twooneb/store', ['uses'=>'TwoonebController@store']);
Route::get('/Twooneb/index', 'TwoonebController@index')->name('Twoonebroutine');
Route::get('/Twooneb/index1', 'TwoonebController@index1');
Route::get('/Twooneb/edit/{id}', ['uses'=>'Twoonebcontroller@view4Edit']);
Route::post('/Twooneb/update', ['uses'=>'Twoonebcontroller@update']);
Route::get('/Twooneb/delete/{id}', ['uses'=>'Twoonebcontroller@delete']);

/*level Two term Two section A*/

Route::get('/Twotwoa/create',function (){

    return view('Twotwoa/create');
});
Route::post('/Twotwoa/store', ['uses'=>'TwotwoaController@store']);
Route::get('/Twotwoa/index', 'TwotwoaController@index')->name('Twotwoaroutine');
Route::get('/Twotwoa/index1', 'TwotwoaController@index1');
Route::get('/Twotwoa/edit/{id}', ['uses'=>'Twotwoacontroller@view4Edit']);
Route::post('/Twotwoa/update', ['uses'=>'Twotwoacontroller@update']);
Route::get('/Twotwoa/delete/{id}', ['uses'=>'Twotwoacontroller@delete']);


/*level Two term Two section B*/

Route::get('/Twotwob/create',function (){

    return view('Twotwob/create');
});
Route::post('/Twotwob/store', ['uses'=>'TwotwobController@store']);
Route::get('/Twotwob/index', 'TwotwobController@index')->name('Twotwobroutine');
Route::get('/Twotwob/index1', 'TwotwobController@index1');
Route::get('/Twotwob/edit/{id}', ['uses'=>'Twotwobcontroller@view4Edit']);
Route::post('/Twotwob/update', ['uses'=>'Twotwobcontroller@update']);
Route::get('/Twotwob/delete/{id}', ['uses'=>'Twotwobcontroller@delete']);