<?php

namespace App\Http\Controllers;


use App\Onetwob;
use Illuminate\Http\Request;

class OnetwobController extends Controller
{
    public function store(){
        $objOnetwob=new Onetwob();
        $objOnetwob->sub_title=$_POST['sub_title'];
        $objOnetwob->teacher_name=$_POST['teacher_name'];
        $objOnetwob->day=$_POST['day'];
        $objOnetwob->time=$_POST['time'];
        $status=$objOnetwob->save();
        return redirect()->route('Onetwobroutine');
    }
    public function index(){
        $objOnetwob=new Onetwob();
        $allData=$objOnetwob->paginate(20);
        return view("Onetwob/index",compact('allData'));
    }
    public function index1(){
        $objOnetwob=new Onetwob();
        $allData=$objOnetwob->paginate(20);
        return view("Onetwob/index1",compact('allData'));
    }
    public function view4Edit($id){
        $objOnetwob=new Onetwob();
        $oneData=$objOnetwob->find($id);
        return view("Onetwob/edit",compact('oneData'));
    }

    public function update(){
        $objOnetwob=new Onetwob();
        $oneData=$objOnetwob->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('Onetwobroutine');
    }
    public function delete($id){
        $objOnetwob=new Onetwob();
        $oneData=$objOnetwob->find($id)->delete();
        return redirect()->route('Onetwobroutine');
    }
}
